//
//  bhtAppDelegate.h
//  TicTacToe
//
//  Created by Johann Hofmann on 31/10/13.
//  Copyright (c) 2013 Johann Hofmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bhtAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
