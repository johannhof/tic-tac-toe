//
//  main.m
//  TicTacToe
//
//  Created by Johann Hofmann on 31/10/13.
//  Copyright (c) 2013 Johann Hofmann. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "bhtAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([bhtAppDelegate class]));
    }
}
